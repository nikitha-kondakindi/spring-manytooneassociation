package com.antra.boot.jpa.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.boot.jpa.entity.Company;
import com.antra.boot.jpa.entity.Professional;
import com.antra.boot.jpa.repository.ProfessionalRepository;

@Component
public class MyCommandLineRunner implements CommandLineRunner {
	@Autowired
	ProfessionalRepository repo;

	@Override
	public void run(String... args) throws Exception {
		Company company = new Company();
		company.setcId(1254);
		company.setAddress("Hyderabad");
		company.setcName("Antra");
		
		Professional prof1 = new Professional();
		prof1.setId(101);   prof1.setName("Miller");  prof1.setExperience(4);
		prof1.setDatoOfBirth("14-2-1992");   prof1.setPhone(9685741232L);
		prof1.setDegree("B.tech");  prof1.setCompany(company);
		
		Professional prof2 = new Professional();
		prof2.setId(102);   prof2.setName("Allen");  prof2.setExperience(5);
		prof2.setDatoOfBirth("20-8-1990");   prof2.setPhone(8659574214L);
		prof2.setDegree("B.tech");   prof2.setCompany(company);
		
		
		repo.save(prof1);
        repo.save(prof2);		

	}

}
