package com.antra.boot.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.antra.boot.jpa.entity.Professional;

public interface ProfessionalRepository extends JpaRepository<Professional, Integer> {

}
