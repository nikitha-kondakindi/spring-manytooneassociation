package com.antra.boot.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Company {
	@Id
	private Integer cId;
	
	@Column(length = 10)
	private String cName;
	
	@Column(length = 15)
	private String address;
	
	public Company() {
		// TODO Auto-generated constructor stub
	}

	public Company(Integer cId, String cName, String address) {
		super();
		this.cId = cId;
		this.cName = cName;
		this.address = address;
	}

	public Integer getcId() {
		return cId;
	}

	public void setcId(Integer cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Company [cId=" + cId + ", cName=" + cName + ", address=" + address +"]";
	}
	
}
