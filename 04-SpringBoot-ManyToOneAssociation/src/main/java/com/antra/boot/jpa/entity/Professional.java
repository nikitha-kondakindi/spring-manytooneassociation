package com.antra.boot.jpa.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Professional {
	@Id
	private Integer id;

	@Column(length = 15)
	private String name;

	private Integer experience;

	@Column(length = 10)
	private String datoOfBirth;

	private Long phone;

	@Column(length = 10)
	private String degree;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CID")
	private Company company;

	public Professional() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	public String getDatoOfBirth() {
		return datoOfBirth;
	}

	public void setDatoOfBirth(String datoOfBirth) {
		this.datoOfBirth = datoOfBirth;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Proffesional [id=" + id + ", name=" + name + ", experience=" + experience + ", datoOfBirth="
				+ datoOfBirth + ", phone=" + phone + ", degree=" + degree + "]";
	}

}
